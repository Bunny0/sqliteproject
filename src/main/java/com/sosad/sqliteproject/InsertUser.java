/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sosad.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bunny0_
 */
public class InsertUser {

    public static void main(String[] args) {
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:user.db");
            c.setAutoCommit(false);
            stmt = c.createStatement();

            String sql = "INSERT INTO USER (ID,username,password)"
                    + "VALUES (NULL,'user3','password')";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,username,password)"
                    + "VALUES (NULL,'user4','password')";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,username,password)"
                    + "VALUES (NULL,'user5','password')";
            stmt.executeUpdate(sql);

            c.commit();
            stmt.close();
            c.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Error Not found");
        } catch (SQLException ex) {
            System.out.println("Unable to connection database");
        }

    }
}
